# Kod för interaktiva rapporter från Nationellt kvalitetsregister för bröstcancer

## Syfte

Skapa webbapplikationer med R-paketen [rccShiny](https://bitbucket.org/cancercentrum/rccshiny), [nkbcgeneral](https://bitbucket.org/cancercentrum/nkbcgeneral) och [nkbcind](https://bitbucket.org/cancercentrum/nkbcind) till interaktiva rapporter från Nationellt kvalitetsregister för bröstcancer (NKBC), http://statistik.incanet.se/brostcancer/.

## Frågor

Kontakta nationellt ansvariga statistiker för NKBC vid Regionalt cancercentrum Stockholm Gotland.
