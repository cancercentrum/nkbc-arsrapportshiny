# Adopted from shinydashboard for NPCR writter by Fredrik Sandin, RCC Uppsala Örebro

library(shiny)
library(shinydashboard)

load("./data/data_list.RData")

server <- shinyServer(function(input, output) {
  # Antal fall
  output$box1 <- renderValueBox({
    valueBox(
      formatC(data_list$antal_fall_report_end_year, format = "d", big.mark = " "),
      paste0("nya fall registrerade i NKBC år ", data_list$report_end_year),
      icon = icon("line-chart"),
      color = "light-blue"
    )
  })
  output$box2 <- renderValueBox({
    valueBox(
      formatC(data_list$antal_fall_std_inkl, format = "d", big.mark = " "),
      paste0("fall under ", data_list$report_start_year, "-", data_list$report_end_year),
      icon = icon("plus"),
      color = "light-blue"
    )
  })

  # Täckningsgrad mot cancerregistret
  output$box3 <- renderValueBox({
    valueBox(
      paste0(data_list$tg_a_perc, " %"),
      paste0("täckningsgrad mot cancerregistret år ", data_list$report_end_year),
      icon = icon("bar-chart"),
      color = data_list$tg_a_color
    )
  })

  # Patienten har erbjudits, i journalen dokumenterad, kontaktsjuksköterska
  output$box4 <- renderValueBox({
    valueBox(
      paste0(data_list$omv_kontaktssk_perc, " %"),
      paste0("erbjöds kontaktsjuksköterska år ", data_list$report_end_year),
      icon = icon("bar-chart"),
      color = data_list$omv_kontaktssk_color
    )
  })

  # Tid från välgrundad misstanke om cancer till primär operation inom 28 dagar
  output$box5 <- renderValueBox({
    valueBox(
      paste0(data_list$ledtid_misstanke_till_op_perc, " %"),
      paste0("där tid från misstanke om cancer till operation är inom 28 dagar ", data_list$report_end_year),
      icon = icon("bar-chart"),
      color = data_list$ledtid_misstanke_till_op_color
    )
  })
})

ui <- shinyUI(
  dashboardPage(
    dashboardHeader(disable = TRUE),
    dashboardSidebar(disable = TRUE),
    dashboardBody(
      includeCSS("styles.css"),
      fluidRow(
        valueBoxOutput("box1"),
        valueBoxOutput("box2")
      ),
      fluidRow(
        valueBoxOutput("box3"),
        valueBoxOutput("box4"),
        valueBoxOutput("box5")
      )
    )
  )
)

shinyApp(ui = ui, server = server)
